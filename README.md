nvm use v12.0.0

npx knex --knexfile knexfile.ts migrate:latest
npx knex --knexfile knexfile.ts seed:run
