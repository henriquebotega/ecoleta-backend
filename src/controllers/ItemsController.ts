import knex from "../database/connection";
import { Request, Response } from "express";

class ItemsController {
	async getAll(req: Request, res: Response) {
		const items = await knex("items").select("*");

		const serializedItems = items.map((i) => {
			return {
				id: i.id,
				title: i.title,
				image_url: `http://10.0.0.7:3333/uploads/${i.image}`,
			};
		});

		return res.json(serializedItems);
	}
}

export default ItemsController;
