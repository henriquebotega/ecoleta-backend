import knex from "../database/connection";
import { Request, Response } from "express";

class PointsController {
	async getAll(req: Request, res: Response) {
		const { city, uf, items } = req.query;

		let parsedItems = [];

		if (items) {
			parsedItems = items.split(",").map((i) => Number(i.trim()));
		}

		const points = await knex("points")
			.join("point_items", "points.id", "=", "point_items.point_id")
			.whereIn("point_items.item_id", parsedItems)
			.where("city", city)
			.where("uf", uf)
			.distinct()
			.select("points.*");

		const serializedPoints = points.map((i) => {
			return {
				...i,
				image_url: `http://10.0.0.7:3333/uploads/${i.image}`,
			};
		});

		return res.json(serializedPoints);
	}

	async getByID(req: Request, res: Response) {
		const { id } = req.params;

		const point = await knex("points").where("id", id).first();

		if (!point) {
			return res.status(400).json({ message: "Not found" });
		}

		const serializedPoints = {
			...point,
			image_url: `http://10.0.0.7:3333/uploads/${point.image}`,
		};

		const items = await knex("items")
			.join("point_items", "items.id", "=", "point_items.item_id")
			.where("point_items.point_id", id)
			.select("items.title");

		return res.json({ serializedPoints, items });
	}

	async create(req: Request, res: Response) {
		const {
			name,
			email,
			whatsapp,
			latitude,
			longitude,
			city,
			uf,
			items,
		} = req.body;

		const trx = await knex.transaction();

		const point = {
			// image: "https://picsum.photos/400",
			image: req.file.filename,
			name,
			email,
			whatsapp,
			latitude,
			longitude,
			city,
			uf,
		};

		const ids = await trx("points").insert(point);

		const pointItems = items
			.split(",")
			.map((i: string) => Number(i.trim()))
			.map((i: number) => {
				return {
					item_id: i,
					point_id: ids[0],
				};
			});

		await trx("point_items").insert(pointItems);

		await trx.commit();

		return res.json({ ...point, id: ids[0] });
	}
}

export default PointsController;
